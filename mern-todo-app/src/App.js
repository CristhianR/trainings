import {BrowserRouter as Router, Route, Link} from 'react-router-dom'
import "bootstrap/dist/css/bootstrap.min.css"

import CreateTodo from "./components/create-todo.component"
import EditTodo from "./components/edit-todo.component"
import TodosList from "./components/todos-list.component"

import logo from './logo.png'

function App() {
    return (
        <Router>
            <div className="container">

            <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
                <div class="container-fluid">
                    <a class="navbar-brand"></a>
                    <a className="navbar-brand">
                        <img src={logo} width="30" height="30"></img>
                    </a>
                    <Link to="/" className="navbar-brand">MERS-Stack Todo App</Link>
                    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                        <ul className="navbar-nav mr-auto">
                            <li className="navbar-item">
                                <Link to="/" className="nav-link">Todos</Link>
                            </li>
                            <li className="navbar-item">
                                <Link to="/create" className="nav-link">Create Todo</Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
                
            
            <Route path="/" exact component = {TodosList} /> 
            <Route path="/edit/:id"  component = {EditTodo} />
            <Route path="/create" component = {CreateTodo} />  
            </div>
        </Router>
    );
  }
  
export default App
