import React, {Component} from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'

const SERVER_URL = 'http://localhost:4000/todos'



const Todo = props => (
    <tr>
        <td className={props.todo.todo_completed ? 'completed' : ''}>{props.todo.todo_description}</td>
        <td className={props.todo.todo_completed ? 'completed' : ''}>{props.todo.todo_responsible}</td>
        <td className={props.todo.todo_completed ? 'completed' : ''}>{props.todo.todo_priority}</td>
        <div>
        <td>
            <Link to={"/edit/"+props.todo._id}>Edit</Link>
        </td>
        <td >
            <Link to={"/"}>Remove</Link>
        </td>
        </div>
    </tr>
)


function onRemove(id) {
    axios.get(`${SERVER_URL}/delete/${id}`)
        .then(res => {
            this.setState({todos: res.data})
        })
        .catch(function (error) {
            console.error(error)
        })
    }

export default class TodosList extends Component {

    constructor(props) {
        super(props)
        this.state = {todos: []}
    }

    componentDidMount() {
        axios.get(SERVER_URL)
            .then(res => {
                this.setState({todos: res.data})
            })
            .catch(function (error) {
                console.error(error)
            })
    }

    todoList() {
        return this.state.todos.map(function(currentTodo, i) {
            return <Todo todo={currentTodo} key={i} />
        });
    }

    componentDidUpdate() {
        axios.get(SERVER_URL)
            .then(res => {
                this.setState({todos: res.data})
            })
            .catch(function (error) {
                console.error(error)
            })
    }


    
    render() {
        return (
            <div>
                <h3>Todos List</h3>
                <table className="table table-striped" style={{ marginTop: 20 }}>
                    <thead>
                        <tr>
                            <th>Description</th>
                            <th>Responsible</th>
                            <th>Priority</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        { this.todoList() }
                    </tbody>
                </table>
            </div>
        )
    }
}
