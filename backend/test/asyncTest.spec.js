/*
Copyright (c) 2021,Cristhian Rojas Fuentes <sec94.kiel.12@gmail.com>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

/* eslint-disable camelcase */
/* eslint max-len: ["error", { "code": 80 }] */

const server = require('../src/server')
const chai = require('chai')
const chaiHttp = require('chai-http')
const expect = chai.expect
const should = chai.should()

const Todo = require('../src/models/todo')

chai.use(chaiHttp)

/**
 * Testing GET/ todos route
 */

describe('/GET todos [async]', () => {
  it('it should GET all the todos', async () => {
    const res = await chai.request(server)
      .get('/todos')
    expect(res.status).to.equal(200)
    expect(res.body).to.be.a('array')
  })
})

/**
 * Testing GET/:id todo route
 * 1: Get the todo by ID
 * 2: Get error by bad ID type
 */

describe('/GET todo [async]', () => {
  it('it should GET one todo', async () => {
    const todo = new Todo({
      todo_description: 'Testing get todo',
      todo_responsible: 'Testing system',
      todo_priority: 'High',
      todo_completed: true
    })
    todo.save(async (err, todo) => {
      if (err) {
        throw new Error(err.message)
      }
      const res = await chai.request(server)
        .get('/todos/' + todo.id)
      expect(res.status).to.equal(200)
      expect(res.body).to.be.a('object')
    })
  })

  it('it should not GET one todo, bad type id [async]', async () => {
    const res = await chai.request(server)
      .get('/todos/12345')
    expect(res.status).to.equal(400)
    expect(res.body).to.be.a('object')
    expect(res.body).to.have.property('errors')
    expect(res.body.errors[0]).to.have.property('msg')
      .equal('id parameters must be a mongo ID.')
  })
})

/**
 * Testing POST/add todo route
 * 1: Create todo
 * 2: Get error by null body param
 * 3: Get error by bad body param type
 */

describe('/POST todo [async]', () => {
  it('it should POST a todo', async () => {
    const todo = {
      todo_description: 'Apply unit test in nodejs',
      todo_responsible: 'Testing system',
      todo_priority: 'Medium',
      todo_completed: false
    }
    const res = await chai.request(server)
      .post('/todos/add')
      .send(todo)
    expect(res.status).to.equal(200)
    expect(res.body).to.be.a('object')
    expect(res.body).to.have.property('msg').equal('todo added successfully')
  })

  it('it should not POST a todo, null description [async]', async () => {
    const todo = {
      todo_description: '',
      todo_responsible: 'Testing system',
      todo_priority: 'Medium',
      todo_completed: false
    }
    const res = await chai.request(server)
      .post('/todos/add')
      .send(todo)
    expect(res.status).to.equal(400)
    expect(res.body).to.be.a('object')
    expect(res.body).to.have.property('errors')
    expect(res.body.errors[0]).to.have.property('msg')
      .equal('Description can not be empty.')
  })

  it('it should not POST a todo, bad description type [async]', async () => {
    const todo = {
      todo_description: 12345,
      todo_responsible: 'Testing system',
      todo_priority: 'Medium',
      todo_completed: false
    }
    const res = await chai.request(server)
      .post('/todos/add')
      .send(todo)
    expect(res.status).to.equal(400)
    expect(res.body).to.be.a('object')
    expect(res.body).to.have.property('errors')
    expect(res.body.errors[0]).to.have.property('msg')
      .equal('Description must be a string type.')
  })
})

/**
 * Testing POST/update/:id todo route
 * 1: Update todo
 * 2: Get error by null body param
 * 3: Get error by bad body param type
 */

describe('/PUT/:id todo [async]', () => {
  it('it should UPDATE a todo given the id', async () => {
    const todo = new Todo({
      todo_description: 'Testing update',
      todo_responsible: 'Testing system',
      todo_priority: 'Low',
      todo_completed: true
    })
    await todo.save((err, todo) => {
      if (err) {
        throw new Error(err.message)
      }
      chai.request(server)
        .post('/todos/update/' + todo.id)
        .send({
          todo_description: 'Testing update',
          todo_responsible: 'Testing system',
          todo_priority: 'High',
          todo_completed: false
        })
        .end((err, res) => {
          if (err) {
            throw new Error(err)
          }
          res.should.have.status(200)
          res.body.should.have.property('msg').eql('Todo update')
        })
    })
  })

  it('it should not UPDATE a todo, null responsible [async]', async () => {
    const todo = new Todo({
      todo_description: 'Testing update',
      todo_responsible: 'Testing system',
      todo_priority: 'Low',
      todo_completed: true
    })
    await todo.save((err, todo) => {
      if (err) {
        throw new Error(err.message)
      }
      chai.request(server)
        .post('/todos/update/' + todo.id)
        .send({
          todo_description: 'Testing update',
          todo_responsible: '',
          todo_priority: 'High',
          todo_completed: false
        })
        .end((err, res) => {
          if (err) {
            throw new Error(err)
          }
          res.should.have.status(400)
          res.body.should.be.a('object')
          res.body.should.have.property('errors')
          res.body.errors[0].should.have.property('msg')
            .eql('Responsible can not be empty.')
        })
    })
  })

  it('it should not UPDATE a todo, bad type responsible [async]', async () => {
    const todo = new Todo({
      todo_description: 'Testing update',
      todo_responsible: 'Testing system',
      todo_priority: 'Low',
      todo_completed: true
    })
    await todo.save((err, todo) => {
      if (err) {
        throw new Error(err.message)
      }
      chai.request(server)
        .post('/todos/update/' + todo.id)
        .send({
          todo_description: 'Testing update',
          todo_responsible: true,
          todo_priority: 'High',
          todo_completed: false
        })
        .end((err, res) => {
          if (err) {
            throw new Error(err)
          }
          res.should.have.status(400)
          res.body.should.be.a('object')
          res.body.should.have.property('errors')
          res.body.errors[0].should.have.property('msg')
            .eql('Responsible must be a string type.')
        })
    })
  })
})

/**
 * Testing GET/:id todo route
 * 1: Get the todo by ID
 * 2: Get error by bad ID type
 */

describe('/DELETE/:id todo [async]', () => {
  it('it should DELETE a todo given the id', async () => {
    const todo = new Todo({
      todo_description: 'Testing delete',
      todo_responsible: 'Unit test system',
      todo_priority: 'Low',
      todo_completed: true
    })
    await todo.save((err, todo) => {
      if (err) {
        throw new Error(err.message)
      }
      chai.request(server)
        .get('/todos/delete/' + todo.id)
        .end((err, res) => {
          if (err) {
            throw new Error(err)
          }
          res.should.have.status(200)
          res.body.should.have.property('msg').eql('Remove successfully')
        })
    })
  })

  it('it should not DELETE a todo, bad type id [async]', async () => {
    const todo = new Todo({
      todo_description: 'Testing delete',
      todo_responsible: 'Unit test system',
      todo_priority: 'Low',
      todo_completed: true
    })
    await todo.save((err, todo) => {
      if (err) {
        throw new Error(err.message)
      }
      chai.request(server)
        .get('/todos/delete/' + 67354)
        .end((err, res) => {
          if (err) {
            throw new Error(err)
          }

          res.should.have.status(400)
          res.body.should.be.a('object')
          res.body.should.have.property('errors')
          res.body.errors[0].should.have.property('msg')
            .eql('id parameters must be a mongo ID')
        })
    })
  })
})
