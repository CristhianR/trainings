/*
Copyright (c) 2021,Cristhian Rojas Fuentes <sec94.kiel.12@gmail.com>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

/* eslint-disable camelcase */
/* eslint max-len: ["error", { "code": 80 }] */

const usersFilter = require('../src/mapFilter')

const expect = require('chai').expect

/**
 * Testing function mapFilter
 * Must be return a new array, filter by age bigger than 50.
 */

describe('users array filter test', () => {
  it('it should be get an array filter by property [ages]', () => {
    const users = [
      {
        name: 'Alejandro Magno',
        age: 33,
        empire: 'Griego'
      },
      {
        name: 'Cayo Octavio Turino',
        age: 75,
        empire: 'Romano'
      },
      {
        name: 'Tutmosis III',
        age: 54,
        empire: 'Egipcio'
      },
      {
        name: 'Ciro II',
        age: 45,
        empire: 'Persa'
      },
      {
        name: 'Asurnasirpal II',
        age: 25,
        empire: 'Asirio'
      },
      {
        name: 'Nabucodonosor II',
        age: 68,
        empire: 'Babilonio'
      }
    ]
    expect(usersFilter.arrayMapping(users)).to.have.length(3)
  })
})
