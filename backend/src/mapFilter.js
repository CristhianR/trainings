/*
Copyright (c) 2021,Cristhian Rojas Fuentes <sec94.kiel.12@gmail.com>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

/* eslint-disable camelcase */
/* eslint max-len: ["error", { "code": 80 }] */

/**
 * Array filter
 * @module mapFilter
 */
/**
  * This function filter an array
  * @function
  * @name arrayMapping
  * @param {array} users - An array users object
  * @property {string} name - User name
  * @property {number} age - User age
  * @property {string} empire - Empire's name
  * @returns {array} filterArray - A new array filter by age
  */

module.exports = {
  arrayMapping: function (users) {
    if (Array.isArray(users)) {
      for (let element = 0; element < users.length; element++) {
        if (!(typeof users[element] === 'object')) {
          return 'array elements must be an object'
        }
        if (typeof users[element].age === 'undefined') {
          return 'elements must have age property'
        }
      }
    } else {
      return 'param must be a array'
    }
    const filterArray = users.filter((user) => user.age >= 50)
    return filterArray
  }
}
