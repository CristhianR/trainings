
/*
Copyright (c) 2021,Cristhian Rojas Fuentes <sec94.kiel.12@gmail.com>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

/* eslint-disable camelcase */
/* eslint max-len: ["error", { "code": 80 }] */

/**
 * Models
 * @module todo-model
 */

const mongoose = require('mongoose')
const Schema = mongoose.Schema

/**
 * Todo
 * @typedef {mongoose.Schema} Todo
 * @property {string} todo_description - A description for todo
 * @property {string} todo_resposible - The name of the todo's responsible
 * @property {string} todo_priority - The todo priority level
 * @property {boolean} todo_completed - Todo completed indicator
 */

const Todo = new Schema({

  todo_description: {
    type: String
  },
  todo_responsible: {
    type: String
  },
  todo_priority: {
    type: String
  },
  todo_completed: {
    type: Boolean
  }
})

module.exports = mongoose.model('Todo', Todo)
