/*
Copyright (c) 2021,Cristhian Rojas Fuentes <sec94.kiel.12@gmail.com>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

/* eslint-disable camelcase */
/* eslint max-len: ["error", { "code": 80 }] */

/**
 * Routers
 * @module todo-routers
 */

const { check } = require('express-validator')

const express = require('express')
const router = express.Router()

const todoController = require('../controllers/todo-controller')

/**
 * Get all the todos objects
 * @name getTodos
 * @path {GET} /
 * @param {function} todoController.getTodos - A controller callback
 */

router.get('/', todoController.getTodos)

/**
 * Get one todo object
 * @name getTodo
 * @path {GET} /:id
 * @param {string} :id Is a mongoDB unique identifier
 * @param {function} todoController.getTodo - A controller callback
 */

router.get('/:id', [
  check('id').notEmpty().withMessage('id parameter can not be empty.')
    .isMongoId().withMessage('id parameters must be a mongo ID.')
], todoController.getTodo)

/**
 * Create a todo object
 * @name postTodo
 * @path {POST} /add
 * @param {function} todoController.createTodo - A controller callback
 */

router.post('/add', [
  check('todo_description').notEmpty()
    .withMessage('Description can not be empty.')
    .isString().withMessage('Description must be a string type.'),
  check('todo_responsible').notEmpty()
    .withMessage('Responsible can not be empty.')
    .isString().withMessage('Responsible must be a string type.'),
  check('todo_priority').notEmpty().withMessage('Priority can not be empty.')
    .isString().withMessage('Priority must be a string type.'),
  check('todo_completed').notEmpty().withMessage('Completed can not be empty.')
    .isBoolean().withMessage('Completed must be a boolean type.')
], todoController.createTodo)

/**
 * Update a todo object
 * @name updateTodo
 * @path {POST} /update/:id
 * @param {string} :id Is a mongoDB unique identifier
 * @param {function} todoController.updateTodo - A controller callback
 */

router.post('/update/:id', [
  check('id').notEmpty().withMessage('id parameter can not be empty')
    .isMongoId().withMessage('id parameters must be a mongo ID'),
  check('todo_description').notEmpty()
    .withMessage('Description can not be empty.')
    .isString().withMessage('Description must be a string type.'),
  check('todo_responsible').notEmpty()
    .withMessage('Responsible can not be empty.')
    .isString().withMessage('Responsible must be a string type.'),
  check('todo_priority').notEmpty().withMessage('Priority can not be empty.')
    .isString().withMessage('Priority must be a string type.'),
  check('todo_completed').notEmpty().withMessage('Completed can not be empty.')
    .isBoolean().withMessage('Completed must be a boolean type.')
], todoController.updateTodo)

/**
 * Delete a todo object
 * @name deleteTodo
 * @path {GET} /delete/:id
 * @param {string} :id Is a mongoDB unique identifier
 * @param {function} todoController.deleteTodo - A controller callback
 */

router.get('/delete/:id', [
  check('id').notEmpty().withMessage('id parameter can not be empty')
    .isMongoId().withMessage('id parameters must be a mongo ID')
], todoController.deleteTodo)

module.exports = router
