/*
Copyright (c) 2021,Cristhian Rojas Fuentes <sec94.kiel.12@gmail.com>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

/* eslint-disable camelcase */
/* eslint max-len: ["error", { "code": 80 }] */

const express = require('express')
const app = express()
const cors = require('cors')
const mongoose = require('mongoose')

/**
 * Express server port number
 * @type {number}
 * @default 4000
 */

const EXPRESS_PORT = 4000

/**
 * Database server IP
 * @type {string}
 * @default '127.0.0.1'
 */

const DB_SERVER = process.env.DB_SERVER || '127.0.0.1'

/**
 * Database server port number
 * @type {number}
 * @default 27017
 */

const DB_PORT = 27017

/**
 * Database collection name
 * @type {string}
 * @default 'todos'
 */

const DB_NAME = 'todos'

// Importing the Todos route
const todoRoutes = require('./routers/todo-router')

app.use(cors())
app.use(express.json())

// Establishing database connection
try {
  mongoose.connect(`mongodb://${DB_SERVER}:${DB_PORT}/${DB_NAME}`,
    { useNewUrlParser: true })
} catch (error) { // On error, close the server
  console.error(error.message)
  process.exit(1)
}

const connection = mongoose.connection

connection.once('open', function () {
  console.log('MongoDB database connection established successfully')
})

app.use('/todos', todoRoutes)

/**
 * Listen on selected port
 * @function
 * @param {number} EXPRESS_PORT - Server port number
 * @return {void}
 */
app.listen(EXPRESS_PORT, function () {
  console.log('Server is running on Port: ' + EXPRESS_PORT)
})

module.exports = app
