/*
Copyright (c) 2021,Cristhian Rojas Fuentes <sec94.kiel.12@gmail.com>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

/* eslint-disable camelcase */
/* eslint max-len: ["error", { "code": 80 }] */

/**
 * Controllers
 * @module todo-controller
 */

/**
  * errors
  * @typedef {Array<object>} errors
  * @property {any} value - A value sended on body property
  * @property {string} msg - A message explaining why the request failure
  * @property {string} param - The property where the error is
  * @property {string} location - The site where the property is
  */

/**
   * message
   * @typedef {object} message
   * @property {string} msg -  A message from server
   */

const { validationResult } = require('express-validator')

const Todo = require('../models/todo')

/**
 * Get all the todos from DB
 * @function
 * @name getTodos
 * @param {object} res - A object promise, will be a Todo's array
 * on success or string on error.
 * @response {object} res - A Todos array object
 * or void if error
 */

exports.getTodos = async (req, res) => {
  const todos = await Todo.find({}).catch(err => {
    console.error(err.message)
  })
  res.json(todos)
}

/**
 * Get one Todo from DB
 * @function
 * @name getTodo
 * @query {any} req - Todo id is type MongoId
 * @param {object} res - A object promise, will be a Todo object
 * on code 200, a errors object array on code 400 and string message on code 500
 * @code {200} If the Todo is found
 * @response {Todo} res - A Todo object
 * @code {400} If the req have errors (bad request)
 * @respose {errors} errors - A errors object array
 * @code {500} If an internal error happen
 * @response {string} res - A error message if an error happens on server
 */

exports.getTodo = async (req, res) => {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() })
  }
  const id = req.params.id
  const todo = await Todo.findById(id).catch(err => {
    console.error(err.message)
    res.status(500).send('getting todo failed')
  })
  res.json(todo)
}

/**
 * Create a todo object on DB
 * @function
 * @name createTodo
 * @param {Todo} req - Todo object
 * @body {string} todo_description - A todo description
 * @body {string} todo_responsible - Name of todo's responsible
 * @body {string} todo_priority - A todo priority level
 * @body {boolean} todo_completed - Todo completed indicator
 * @param {object} res - A object promise, will be a Todo's object array
 * on code 200, a errors object array on code 400 and string message on code 500
 * @code {200} If the object is created
 * @response {object} res - A success message object
 * @code {400} If the req have errors (bad request)
 * @respose {errors} res - A errors object array
 * @code {500} If an internal error happen
 * @response {message} res - A error message if an error happens on server
 */

exports.createTodo = async (req, res) => {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() })
  }
  const todo = new Todo(req.body)
  await todo.save().then(message => {
    res.status(200).json({ msg: 'todo added successfully' })
    res.send(message)
  })
    .catch(err => {
      console.error(err.message)
      res.status(500).json({ msg: 'adding new todo failed' })
    })
}

/**
 * Update a todo object on DB
 * @function
 * @name updateTodo
 * @query {any} req - Todo id is type MongoId
 * @body {string} todo_description - A todo description
 * @body {string} todo_responsible - Name of todo's responsible
 * @body {string} todo_priority - A todo priority level
 * @body {boolean} todo_completed - Todo completed indicator
 * @param {object} res - A object promise, will be a message object
 * on code 200, a errors object array on code 400 and string message
 * on code 500
 * @code {200} If the object is update
 * @response {message} res - A success message object
 * @code {400} If the req have errors (bad request)
 * @respose {errors} res - A errors object array
 * @code {500} If an internal error happen
 * @response {string} res - A error message if an error happens on server
 * @code {404} If the todo is not found
 * @respose {message} res - A error message
 */

exports.updateTodo = async (req, res) => {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() })
  }
  await Todo.findById(req.params.id, function (err, todo) {
    if (err) {
      console.error(err.message)
    }
    if (!todo) {
      res.status(404).json({ msg: 'Data is not found' })
    } else {
      const keys = ['todo_description', 'todo_responsible', 'todo_priority',
        'todo_completed']
      keys.forEach(key => { todo[key] = req.body[key] })

      todo.save().then(todo => {
        res.json({ msg: 'Todo update' })
      })
        .catch(err => {
          console.error(err.message)
          res.status(500).json({ msg: 'Update not possible' })
        })
    }
  })
}

/**
 * Delete one Todo from DB
 * @function
 * @name deleteTodo
 * @query {any} req - Todo id is type MongoId
 * @param {object} res - A object promise, will be a message object,
 * success on code 200 or error on code 500, an errors object array on code 400
 * @code {200} If the Todo is deleted
 * @response {message} res - A success message object
 * @code {400} If the req have errors (bad request)
 * @respose {errors} errors - A errors object array
 * @code {500} if an internal error happen
 * @response {mesaage} res - A error message object
 */

exports.deleteTodo = (req, res) => {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() })
  }
  Todo.findByIdAndRemove({ _id: req.params.id }, (err) => {
    if (err) {
      res.status(500).json(
        { msg: 'Error while finding and removing the element' })
    } else {
      res.json({ msg: 'Remove successfully' })
    }
  })
}
